2020.06.00
  - Initial release.
    - Supports LAPACK >= 3.2.1.
    - Includes routines through LAPACK 3.7.0.
